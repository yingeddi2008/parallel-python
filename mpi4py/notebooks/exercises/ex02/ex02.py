from mpi4py import MPI 
comm = MPI.COMM_WORLD 
rank = comm.Get_rank() 
#
# Run on 2 proc.
# Assign the integer variable myint = 0 for rank 0, 1 for rank 1.
# Check by writing that assignment is correct
# Initialize to 0 an integer variable called received_int which will contain the variable of
# myint owned by the other rank.
# Use MPI_SEND / MPI_RECV to exchange the value of myint.
# Have each rank WRITE the value of myint and received_int and check that rank 0 write
# 0,1 while rank 1 writes 1,0.
#=====

#=====
