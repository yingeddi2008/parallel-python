# Distributed-memory Parallel Programming with MPI in Python Workshop (mpi4py)

Materials for the Distributed-memory Parallel Programming with MPI in Python Workshop (Winter 2019)

## Objectives
Learning Outcomes for attendees of this workshop:

* Able to understand the differences between shared memory and distributed memory processing and known when each is appropriate
* Understand the MPI concepts of point-to-point communication, collective communication, and one-sided communication
* Be able to write a simple python program using MPI
* Know how to setup your python environment to utilize mpi4py package and be able to execute such jobs on the cluster

## Outline

* [Parallel Computing Overview and Background on the Message Passing Interface](pdfs/overview.pdf)

* [Lab1:](notebooks/labs/lab1/lab1.ipynb) Simple working MPI example -- Hello world 

* [Lab2:](notebooks/labs/lab2/lab2.ipynb) Point-to-Point Communications (Send | Recv)

* [Lab3:] Blocking vs Non-Blocking

* [Lab4:] Collective Communications
 
* [Lab5:] Collective Communications 2

* [Lab6:] Real Example of MPI Parallel Code

* Additional Exercises -- See the exercises folder

## Running the Notebooks for this module

Open the `master.ipynb` notebook to work through this module. 

