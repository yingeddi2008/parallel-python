# Parallel Programming in Python:

Material for the threading and multiprocessing in python workshop.

## Objectives
Learning Outcomes for attendees of this workshop:

* Able to understand the differences between shared memory and distributed memory processing and known when each is appropriate
* Know how to setup your python environment to utilize mpi4py package and be able to execute such jobs on the cluster
* Able to understand the differences between multithreading and multiprocessing.
* Know which tasks are better suited for multithreading or multiprocessing
* Able to use threading and multiprocessing packages for appropriately suited tasks.
* Be able to write a simple python program using MPI
* Understand the MPI concepts of point-to-point communication, collective communication, and one-sided communication



## Outline
* Overview:
  * [Jupyter Lab IDE & Notebook Basics](notebooks/jupyter_intro.ipynb)
  * [Overview of parallel computing with python](notebooks/overview.ipynb)
* Threading:
  * c-extension thread enabled libraries (numpy & scipy)
  * threading package
* Multiprocessing:
  * Process class
  * Pool class
* Other parallel libraries:
  * [pymp -- OpenMP-like functionality for Python](notebooks/pymp.ipynb)

## Running the Notebooks for this Workshop

We will use Midway compute nodes to run the lab notebooks and examples. In order to do
so you should log on to Midway. If you do not have an account or this is your first time
accessing midway, see the [Connecting to Midway Document ](pdfs/connecting_midway.pdf). 

Once logged on to Midway, if you do not want to run the notebooks from your home directory, 
change directory to a location of your choice. It is generally recommended that  
compute jobs are run from your SCRATCH directory (/scratch/midway2/$USER).  

Then clone this repository:

```bash
git clone https://gitlab.com/uchicago-rcc/parallel-python.git
```

We will use a jupyter lab IDE to run the workshop notebooks. 
Change directory into the repository directory and run the `launch-jlab.sh` 
script to start a jupyter lab session on a compute node.

```bash
cd parallel-python
/bin/sh ./launch-jlab.sh
```

After you are connected locally to the jupyter lab IDE, open the multiprocessing folder
and launch the `master.ipynb` notebook.

The launch-jlab.sh script will prompt you with instructions for
connecting to the jupyter lab session created for you on the compute nodes.If you are on
the UofC campus network, you should be able to directly copy the first URL link printed
into your local (i.e. laptop) web browser and connect to the running jupyter lab session.
Note that if one is not on the campus network, but would prefer to access the jupyter lab
session in this manner, one needs to first connect to the campus VPN first, before running
the `launch.nb` script. If not on the campus network, you will have two steps to follow
in order to successfully connect to the running jupyter lab session on the midway compute
node. Follow the two-step instructions printed by the launch.nb script, where you must
first tunnel with the remote server and then launch your jupyter lab session on your
local machine using the `localhost` address as displayed in the output from launch-nb.sh


Additional Information on the jupyter-lab launch script can be found at the following link:
  * [Juypter lab launch script repo](https://git.rcc.uchicago.edu/jhskone/jupyter-lab)
Outside of this workshop the script can be reused for your own use with some minor modifications.


